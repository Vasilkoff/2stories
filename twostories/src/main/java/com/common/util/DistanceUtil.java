package com.common.util;

import com.getyasa.TwoStories;

public class DistanceUtil {

    public static int getCameraAlbumWidth() {
        return (TwoStories.getApp().getScreenWidth() - TwoStories.getApp().dp2px(10)) / 4 - TwoStories.getApp().dp2px(4);
    }
    
    // Camera photo list height calculation
    public static int getCameraPhotoAreaHeight() {
        return getCameraPhotoWidth() + TwoStories.getApp().dp2px(4);
    }
    
    public static int getCameraPhotoWidth() {
        return TwoStories.getApp().getScreenWidth() / 4 - TwoStories.getApp().dp2px(2);
    }

    //Events tab grid Image Height
    public static int getActivityHeight() {
        return (TwoStories.getApp().getScreenWidth() - TwoStories.getApp().dp2px(24)) / 3;
    }
}
