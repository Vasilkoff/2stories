package com.getyasa.app.camera;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;

import com.getyasa.activities.ApplyEffectsActivity;
import com.getyasa.activities.Camera.Utility.Constant;
import com.getyasa.app.camera.ui.AlbumActivity;
import com.getyasa.app.camera.ui.CameraActivity;
import com.getyasa.app.camera.ui.MyListener;
import com.getyasa.app.model.PhotoItem;

import java.io.IOException;
import java.util.Stack;

/**
 * Camera Management
 * Created by sky on 15/7/6.
 */
public class CameraManager {

    private static CameraManager mInstance;
    private Stack<Activity> cameras = new Stack<Activity>();

    public static CameraManager getInst() {
        if (mInstance == null) {
            synchronized (CameraManager.class) {
                if (mInstance == null)
                    mInstance = new CameraManager();
            }
        }
        return mInstance;
    }

    //Open the camera interface
    public void openCamera(Context context) {
        Intent intent = new Intent(context, CameraActivity.class);
        context.startActivity(intent);
    }


    public Bitmap processPhotoItem(Activity activity, PhotoItem photo) {
        Uri uri = photo.getImageUri().startsWith("file:") ? Uri.parse(photo
                .getImageUri()) : Uri.parse("file://" + photo.getImageUri());

        try {
            Constant.bitmap = MediaStore.Images.Media.getBitmap(activity.getContentResolver(), uri);

            //Intent newIntent = new Intent(activity, com.getyasa.activities.CameraActivity.class);
            //newIntent.putExtra("imageGallery", true);
            //activity.startActivity(newIntent);

        } catch (IOException e) {
            e.printStackTrace();
        }

        return Constant.bitmap;
        /*Intent newIntent = new Intent(activity, com.getyasa.activities.CameraActivity.class);
        newIntent.setData(uri);
        activity.startActivity(newIntent);*/

//        Uri uri = photo.getImageUri().startsWith("file:") ? Uri.parse(photo
//                .getImageUri()) : Uri.parse("file://" + photo.getImageUri());
//        if (ImageUtils.isSquare(photo.getImageUri())) {
//            Intent newIntent = new Intent(activity, ApplyEffectsActivity.class);
//            newIntent.setData(uri);
//            activity.startActivity(newIntent);
//        } else {
//            Intent i = new Intent(activity, CropPhotoActivity.class);
//            i.setData(uri);
//            //TODO稍后添加
//            activity.startActivityForResult(i, AppConstants.REQUEST_CROP);
//        }
    }

    public void close() {
        for (Activity act : cameras) {
            try {
                act.finish();
            } catch (Exception e) {

            }
        }
        cameras.clear();
    }

    public void addActivity(Activity act) {
        cameras.add(act);
    }

    public void removeActivity(Activity act) {
        cameras.remove(act);
    }



}
