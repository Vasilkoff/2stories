package com.getyasa.app.camera.fragment;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.Toast;

import com.getyasa.R;
import com.getyasa.YasaConstants;
import com.getyasa.app.camera.adapter.MyGridListAdapter;
import com.getyasa.app.lazyload.ImageLoader;
import com.getyasa.app.model.PhotoItem;
import com.getyasa.socialnetwork.InstagramApp;
import com.getyasa.socialnetwork.InstagramSession;
import com.getyasa.socialnetwork.JSONParser;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Admin on 01.02.16.
 */
public class InstagramFragment extends Fragment {

    private static Button btnConnect;

    private InstagramSession mSession;

    private GridView gvAllImages;
    private ProgressDialog pd;
    private int WHAT_FINALIZE = 0;
    private static int WHAT_ERROR = 1;
    private HashMap<String, String> userInfo;
    private ImageLoader imageLoader;

    private Context mContext;

    private ArrayList<String> imageThumbList = new ArrayList<String>();

    private static InstagramApp mApp;

    public static final String TAG_DATA = "data";
    public static final String TAG_IMAGES = "images";
    public static final String TAG_THUMBNAIL = "thumbnail";
    public static final String TAG_URL = "url";

    public interface onSomeEventListener {
        public void someEvent(Bitmap image);
    }

    onSomeEventListener someEventListener;

    public InstagramFragment() {
        super();
    }

    public static Fragment newInstance() {
        Fragment fragment = new InstagramFragment();

        return fragment;
    }

    private void connectOrDisconnectUser() {
        if (mApp.hasAccessToken()) {
            final AlertDialog.Builder builder = new AlertDialog.Builder(
                    mContext);
            builder.setMessage("Disconnect from Instagram?")
                    .setCancelable(false)
                    .setPositiveButton("Yes",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,
                                                    int id) {
                                    mApp.resetAccessToken();
                                    // btnConnect.setVisibility(View.VISIBLE);
                                    //llAfterLoginView.setVisibility(View.GONE);
                                    btnConnect.setText("Connect");
                                    // tvSummary.setText("Not connected");
                                }
                            })
                    .setNegativeButton("No",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,
                                                    int id) {
                                    dialog.cancel();
                                }
                            });
            final AlertDialog alert = builder.create();
            alert.show();
        } else {
            mApp.authorize();
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            someEventListener = (onSomeEventListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement onSomeEventListener");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.instagram_fragment,
                container, false);

        mContext = container.getContext();

        imageLoader = new ImageLoader(mContext);

        gvAllImages = (GridView) view.findViewById(R.id.gvAllImages);
        gvAllImages.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String url_image = imageThumbList.get(position);

                Bitmap image = imageLoader.getBitmap(url_image);
                someEventListener.someEvent(image);
            }
        });

        mApp = new InstagramApp(container.getContext(), YasaConstants.CLIENT_ID,
                YasaConstants.CLIENT_SECRET, YasaConstants.CALLBACK_URL);
        mApp.setListener(new InstagramApp.OAuthAuthenticationListener() {

            @Override
            public void onSuccess() {
                // tvSummary.setText("Connected as " + mApp.getUserName());
                btnConnect.setText("Disconnect");
                //llAfterLoginView.setVisibility(View.VISIBLE);
                // userInfoHashmap = mApp.
                //mApp.fetchUserName(handler);

                userInfo = mApp.getUserInfo();
                getAllMediaImages(container.getContext());
            }

            @Override
            public void onFail(String error) {
                Toast.makeText(container.getContext(), error, Toast.LENGTH_SHORT)
                        .show();
            }
        });

        mSession = new InstagramSession(mContext);

        btnConnect = (Button) view.findViewById(R.id.btnConnect);
        btnConnect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                connectOrDisconnectUser();
            }
        });

        if (mApp.hasAccessToken()) {
            // tvSummary.setText("Connected as " + mApp.getUserName());
            btnConnect.setText("Disconnect");
            userInfo = mApp.getUserInfo();
            getAllMediaImages(container.getContext());
            //llAfterLoginView.setVisibility(View.VISIBLE);
            //mApp.fetchUserName(handler);

        }

        return view;
    }

    public static Bitmap getBitmapFromURL(String src) {
        try {
            URL url = new URL(src);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);
            return myBitmap;
        } catch (IOException e) {
            // Log exception
            Log.e("Exeption", String.valueOf(e));
            return null;
        }
    }

    private void getAllMediaImages(Context context) {
        pd = ProgressDialog.show(context, "", "Loading images...");
        new Thread(new Runnable() {

            @Override
            public void run() {
                int what = WHAT_FINALIZE;
                try {
                    // URL url = new URL(mTokenUrl + "&code=" + code);
                    String str = "https://api.instagram.com/v1/users/"
                            + mSession.getId()
                            + "/media/recent/?client_id="
                            + YasaConstants.CLIENT_ID
                            + "&count="
                            + null;
                    JSONParser jsonParser = new JSONParser();
                    JSONObject jsonObject = jsonParser
                            .getJSONFromUrlByGet("https://api.instagram.com/v1/users/"
                                    + mSession.getId()
                                    + "/media/recent/?access_token="
                                    + mSession.getAccessToken ()
                                    + "&count="
                                    + null);
                    JSONArray data = jsonObject.getJSONArray(TAG_DATA);
                    for (int data_i = 0; data_i < data.length(); data_i++) {
                        JSONObject data_obj = data.getJSONObject(data_i);

                        JSONObject images_obj = data_obj
                                .getJSONObject(TAG_IMAGES);

                        JSONObject thumbnail_obj = images_obj
                                .getJSONObject(TAG_THUMBNAIL);

                        // String str_height =
                        // thumbnail_obj.getString(TAG_HEIGHT);
                        //
                        // String str_width =
                        // thumbnail_obj.getString(TAG_WIDTH);

                        String str_url = thumbnail_obj.getString(TAG_URL);
                        imageThumbList.add(str_url);
                    }

                    System.out.println("jsonObject::" + jsonObject);

                } catch (Exception exception) {
                    exception.printStackTrace();
                    what = WHAT_ERROR;
                }
                // pd.dismiss();
                handler.sendEmptyMessage(what);
            }
        }).start();
    }

    private Handler handler = new Handler(new Handler.Callback() {

        @Override
        public boolean handleMessage(Message msg) {
            if (pd != null && pd.isShowing())
                pd.dismiss();
            if (msg.what == WHAT_FINALIZE) {
                setImageGridAdapter();
            } else {
                Toast.makeText(mContext, "Check your network.",
                        Toast.LENGTH_SHORT).show();
            }
            return false;
        }
    });

    private void setImageGridAdapter() {
        gvAllImages.setAdapter(new MyGridListAdapter(mContext, imageThumbList));
    }

}
