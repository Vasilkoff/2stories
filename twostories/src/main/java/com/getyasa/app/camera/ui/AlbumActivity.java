package com.getyasa.app.camera.ui;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.hardware.camera2.params.Face;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.ArrayMap;
import android.view.View;

import com.common.util.FileUtils;
import com.common.util.ImageUtils;
import com.common.util.StringUtils;
import com.customview.PagerSlidingTabStrip;
import com.getyasa.YasaConstants;
import com.getyasa.R;
import com.getyasa.activities.Camera.Utility.Constant;
import com.getyasa.app.camera.CameraBaseActivity;
import com.getyasa.app.camera.fragment.AlbumFragment;
import com.getyasa.app.camera.fragment.FacebookFragment;
import com.getyasa.app.camera.fragment.InstagramFragment;
import com.getyasa.app.model.Album;
import com.getyasa.app.model.PhotoItem;
import com.getyasa.base.YasaBaseActivity;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import butterknife.ButterKnife;
import butterknife.InjectView;

public class AlbumActivity extends YasaBaseActivity implements AlbumFragment.onSomeEventListener, InstagramFragment.onSomeEventListener {

    private Map<String, Album> albums;
    private List<String> paths = new ArrayList<String>();

    @InjectView(R.id.indicator)
    PagerSlidingTabStrip tab;
    @InjectView(R.id.pager)
    ViewPager pager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_album);
        ButterKnife.inject(this);
        albums = ImageUtils.findGalleries(this, paths, 0);
        albums.put("YASA Instagram", new Album("YASA Instagram", "", new ArrayList<PhotoItem>()));
        albums.put("YASA Facebook", new Album("YASA Facebook", "", new ArrayList<PhotoItem>()));
        paths.add("YASA Instagram");
        paths.add("YASA Facebook");
        //ViewPager adapter
        FragmentPagerAdapter adapter = new TabPageIndicatorAdapter(getSupportFragmentManager());
        pager.setAdapter(adapter);
        tab.setViewPager(pager);

        setUpActionBar(true,false,"Gallery");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onActivityResult(final int requestCode, final int resultCode, final Intent result) {
        if (requestCode == YasaConstants.REQUEST_CROP && resultCode == RESULT_OK) {

        }
    }

    @Override
    public void someEvent(Bitmap image) {
        Constant.bitmap = image;
        //Uri uri = getImageUri(getApplicationContext(), image);
        Intent i = new Intent();
        i.putExtra("imageFromGallery","imageFromGallery");
        setResult(RESULT_OK, i);
        finish();
    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }


    class TabPageIndicatorAdapter extends FragmentPagerAdapter {
        public TabPageIndicatorAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            //New to display the contents of a Fragment ViewPager item and pass parameters
            String title = albums.get(paths.get(position)).getTitle();
            ArrayList<PhotoItem> photos = albums.get(paths.get(position)).getPhotos();
            if (title.equals("YASA Instagram")) {
                return InstagramFragment.newInstance();
            } else if (title.equals("YASA Facebook")) {
                return FacebookFragment.newInstance();
            } else {
                return AlbumFragment.newInstance(photos);
            }
        }

        @Override
        public CharSequence getPageTitle(int position) {
            Album album = albums.get(paths.get(position % paths.size()));
            if (StringUtils.equalsIgnoreCase(FileUtils.getInst().getSystemPhotoPath(),
                    album.getAlbumUri())) {
                return getString(R.string.tab_gallery);
            } else if (album.getTitle().length() > 13) {
                return album.getTitle().substring(0, 11) + "...";
            }
            return album.getTitle();
        }

        @Override
        public int getCount() {
            return paths.size();
        }
    }

    private void close() {
        finish();
    }

}
