package com.getyasa.activities;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.hardware.Camera;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.common.util.FileUtils;
import com.common.util.ImageUtils;
import com.common.util.TimeUtils;
import com.customview.MyImageViewDrawableOverlay;
import com.getyasa.FiltersAdapter;
import com.getyasa.R;
import com.getyasa.YasaConstants;
import com.getyasa.activities.Camera.PreviewSurfaceView;
import com.getyasa.activities.Camera.Task.ImageDecodeTask;
import com.getyasa.activities.Camera.Utility.BitmapHelper;
import com.getyasa.activities.Camera.Utility.Constant;
import com.getyasa.app.camera.ui.AlbumActivity;
import com.getyasa.base.YasaBaseActivity;
import com.getyasa.collage.MultiTouchListener;
import com.github.siyamed.shapeimageview.CircularImageView;
import com.github.siyamed.shapeimageview.DiamondImageView;
import com.github.siyamed.shapeimageview.HeartImageView;
import com.github.siyamed.shapeimageview.HexagonImageView;
import com.github.siyamed.shapeimageview.OctogonImageView;
import com.github.siyamed.shapeimageview.StarImageView;

import java.io.File;
import java.util.Date;
import java.util.List;

/**
 * Created by maxim.vasilkov@gmail.com on 17/12/15.
 */
public class CameraActivity extends YasaBaseActivity {

    private static final int CAMERA_SHAPE = 1899;

    private static final int DIAMOND_IMAGE = 3;
    private static final int HEXAGON_IMAGE = 4;
    private static final int HEART_IMAGE = 5;
    private static final int CIRCULAR_IMAGE = 6;
    private static final int OCTOGON_IMAGE = 7;
    private static final int STAR_IMAGE = 8;

    private Camera camera;
    private PreviewSurfaceView previewSurfaceView;
    private FrameLayout previewFrame;
    private Button captureButton;
    private ImageView switchCameraButton;
    private ImageView galleryButton;
    String shape_id;
    private RelativeLayout shapeToolbar;
    private RelativeLayout cameraToolbar;

    Boolean imageGallery;

    private ImageView imageFull;
    private RelativeLayout cameraPreview;
    private RelativeLayout cameraTakeImage;

    MenuItem flashBtn;
    boolean hasFlash;

    RecyclerView mRecyclerView;

    private RecyclerView.LayoutManager mLayoutManager;

    private MyImageViewDrawableOverlay mImageView;

    int[] stickers = {
            R.drawable.shape3,
            R.drawable.shape4,
            R.drawable.shape5,
            R.drawable.shape6,
            R.drawable.shape7,
            R.drawable.shape8
    };

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate menu resource file.
        getMenuInflater().inflate(R.menu.menu_camera_flash, menu);

        flashBtn = menu.findItem(R.id.action_flash);

        hasFlash = isHasFlash();
        flashBtn.setVisible(hasFlash);

        // Return true to display menu
        return true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Boolean front = getIntent().getBooleanExtra("front",false);
        Constant.BACK_CAMERA_IN_USE = !front;

            shape_id = getIntent().getStringExtra("shape_id");
            switch (shape_id) {

                case "3": {
                    if (front) {
                        setContentView(R.layout.activity_camera_surface3_);
                    } else {
                        setContentView(R.layout.activity_camera_surface);
                    }
                }
                break;
                case "4": {
                    if (front) {
                        setContentView(R.layout.activity_camera_surface4_);
                    } else {
                        setContentView(R.layout.activity_camera_surface);
                    }
                }
                break;
                case "5": {
                    if (front) {
                        setContentView(R.layout.activity_camera_surface5_);
                    } else {
                        setContentView(R.layout.activity_camera_surface);
                    }
                }
                break;
                case "6": {
                    if (front) {
                        setContentView(R.layout.activity_camera_surface6_);
                    } else {
                        setContentView(R.layout.activity_camera_surface);
                    }
                }
                break;
                case "7": {
                    if (front) {
                        setContentView(R.layout.activity_camera_surface7_);
                    } else {
                        setContentView(R.layout.activity_camera_surface);
                    }
                }
                break;
                case "8": {
                    if (front) {
                        setContentView(R.layout.activity_camera_surface8_);
                    } else {
                        setContentView(R.layout.activity_camera_surface);
                    }
                }
                break;
                default: {
                    setContentView(R.layout.activity_camera_surface);

                    mLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
                    mRecyclerView = (RecyclerView) findViewById(R.id.my_recycler_view);
                    mRecyclerView.setLayoutManager(mLayoutManager);

                    mRecyclerView.setAdapter(new FiltersAdapter(stickers, stickerAddHandler));

                    shapeToolbar = (RelativeLayout) findViewById(R.id.shape_toolbar);
                    cameraToolbar = (RelativeLayout) findViewById(R.id.camera_toolbar);

                    shapeToolbar.setVisibility(View.GONE);
                    cameraToolbar.setVisibility(View.VISIBLE);

                    imageFull = (ImageView) findViewById(R.id.image);
                    imageFull.setOnTouchListener(new MultiTouchListener());

                    cameraPreview = (RelativeLayout) findViewById(R.id.cameraPreview);
                    cameraTakeImage = (RelativeLayout) findViewById(R.id.cameraTakeImage);

                    cameraPreview.setVisibility(View.VISIBLE);
                    cameraTakeImage.setVisibility(View.GONE);
                }
            }


        int sid = getIntent().getIntExtra("shape_id", 0);

        if (sid!=0)
            setUpActionBar(false,true,"");
        else
            setUpActionBar(false,false,"");

        previewFrame = (FrameLayout)findViewById(R.id.camera_preview);
        captureButton = (Button)findViewById(R.id.button_capture);
        switchCameraButton = (ImageView)findViewById(R.id.button_switch);
        galleryButton = (ImageView)findViewById(R.id.gallery);

        captureButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    camera.takePicture(null, null, pictureCallback);
                    if (shapeToolbar != null)
                        shapeToolbar.setVisibility(View.VISIBLE);
                    if (cameraToolbar != null)
                        cameraToolbar.setVisibility(View.GONE);
                    setUpActionBar(false,true,"");
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        galleryButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent customAlbumActivity = new Intent(CameraActivity.this, AlbumActivity.class);
                customAlbumActivity.setType("image/*");
                startActivityForResult(customAlbumActivity, YasaConstants.REQUEST_PICK);

               /* Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
                photoPickerIntent.setType("image/*");
                startActivityForResult(photoPickerIntent, YasaConstants.REQUEST_PICK); */
            }
        });

        switchCameraButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Constant.BACK_CAMERA_IN_USE) {
                    showFrontCamera();
                } else {
                    showBackCamera();
                }
            }
        });

        showBackCamera();
    }

    private Handler stickerAddHandler = new Handler(new Handler.Callback() {
        @Override
        public boolean handleMessage(Message msg) {

            Log.e("MSG", String.valueOf(msg.arg2));


            Constant.isShapeBitmap = true;

            Intent intent = new Intent(CameraActivity.this, CameraActivity.class);
            intent.putExtra("shape_id", String.valueOf(msg.arg2+3));
            Constant.NUMBER_OF_SHAPE = msg.arg2+3;
            intent.putExtra("front", true);
            overridePendingTransition(R.anim.appear_bottom_right_in, R.anim.disappear_top_left_out);
            startActivityForResult(intent, CAMERA_SHAPE);

            return false;

        }
    });



    /**
     * Flash switch On -> Off -> Automatic
     *
     * @param mCamera
     */
    private void turnLight(Camera mCamera) {
        if (mCamera == null || mCamera.getParameters() == null
                || mCamera.getParameters().getSupportedFlashModes() == null) {
            return;
        }
        Camera.Parameters parameters = mCamera.getParameters();
        String flashMode = mCamera.getParameters().getFlashMode();
        List<String> supportedModes = mCamera.getParameters().getSupportedFlashModes();
        if (Camera.Parameters.FLASH_MODE_OFF.equals(flashMode)
                && supportedModes.contains(Camera.Parameters.FLASH_MODE_ON)) {
            parameters.setFlashMode(Camera.Parameters.FLASH_MODE_ON);
            mCamera.setParameters(parameters);
            flashBtn.setIcon(R.drawable.camera_flash_on);
        } else if (Camera.Parameters.FLASH_MODE_ON.equals(flashMode)) {
            if (supportedModes.contains(Camera.Parameters.FLASH_MODE_AUTO)) {
                parameters.setFlashMode(Camera.Parameters.FLASH_MODE_AUTO);
                flashBtn.setIcon(R.drawable.camera_flash_auto);
                mCamera.setParameters(parameters);
            } else if (supportedModes.contains(Camera.Parameters.FLASH_MODE_OFF)) {
                parameters.setFlashMode(Camera.Parameters.FLASH_MODE_OFF);
                flashBtn.setIcon(R.drawable.camera_flash_off);
                mCamera.setParameters(parameters);
            }
        } else if (Camera.Parameters.FLASH_MODE_AUTO.equals(flashMode)
                && supportedModes.contains(Camera.Parameters.FLASH_MODE_OFF)) {
            parameters.setFlashMode(Camera.Parameters.FLASH_MODE_OFF);
            mCamera.setParameters(parameters);
            flashBtn.setIcon(R.drawable.camera_flash_off);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_flash:
                turnLight(camera);
                return true;
        }
        return false;
    }

    @Override
    protected void onActivityResult(final int requestCode, final int resultCode, final Intent result) {
        if (requestCode == YasaConstants.REQUEST_PICK && resultCode == RESULT_OK) {

            if (shapeToolbar != null)
                shapeToolbar.setVisibility(View.VISIBLE);
            if (cameraToolbar != null)
                cameraToolbar.setVisibility(View.GONE);

            setUpActionBar(false,true,"");

            if (cameraPreview == null)
                cameraPreview = (RelativeLayout) findViewById(R.id.cameraPreview);
            if (cameraTakeImage == null)
                cameraTakeImage = (RelativeLayout) findViewById(R.id.cameraTakeImage);

            cameraPreview.setVisibility(View.GONE);
            cameraTakeImage.setVisibility(View.VISIBLE);

            imageFull.setImageBitmap(Constant.bitmap);

            return;

        } else if (requestCode == CAMERA_SHAPE && resultCode == RESULT_OK) {

            switch (Constant.NUMBER_OF_SHAPE) {
                case DIAMOND_IMAGE: {
                    DiamondImageView image = new DiamondImageView(this);

                    image.setOnTouchListener(new MultiTouchListener());

                    image.setImageBitmap(Constant.bitmap);

                    LinearLayout.LayoutParams vp =
                            new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,
                                    LinearLayout.LayoutParams.WRAP_CONTENT);

                    image.setLayoutParams(vp);

                    cameraTakeImage.addView(image);

                    break;
                }
                case HEXAGON_IMAGE: {
                    HexagonImageView image = new HexagonImageView(this);

                    image.setOnTouchListener(new MultiTouchListener());

                    image.setImageBitmap(Constant.bitmap);

                    LinearLayout.LayoutParams vp =
                            new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,
                                    LinearLayout.LayoutParams.WRAP_CONTENT);

                    image.setLayoutParams(vp);

                    cameraTakeImage.addView(image);

                    break;
                }
                case HEART_IMAGE: {

                    HeartImageView image = new HeartImageView(this);

                    image.setOnTouchListener(new MultiTouchListener());

                    image.setImageBitmap(Constant.bitmap);

                    LinearLayout.LayoutParams vp =
                            new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,
                                    LinearLayout.LayoutParams.WRAP_CONTENT);

                    image.setLayoutParams(vp);

                    cameraTakeImage.addView(image);

                    break;
                }
                case CIRCULAR_IMAGE: {

                    CircularImageView image = new CircularImageView(this);

                    image.setOnTouchListener(new MultiTouchListener());

                    image.setImageBitmap(Constant.bitmap);

                    LinearLayout.LayoutParams vp =
                            new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,
                                    LinearLayout.LayoutParams.WRAP_CONTENT);

                    image.setLayoutParams(vp);

                    cameraTakeImage.addView(image);

                    break;
                }
                case OCTOGON_IMAGE: {

                    OctogonImageView image = new OctogonImageView(this);

                    image.setOnTouchListener(new MultiTouchListener());

                    image.setImageBitmap(Constant.bitmap);

                    LinearLayout.LayoutParams vp =
                            new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,
                                    LinearLayout.LayoutParams.WRAP_CONTENT);

                    image.setLayoutParams(vp);

                    cameraTakeImage.addView(image);

                    break;
                }
                case STAR_IMAGE: {
                    StarImageView image = new StarImageView(this);

                    image.setOnTouchListener(new MultiTouchListener());

                    image.setImageBitmap(Constant.bitmap);

                    LinearLayout.LayoutParams vp =
                            new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,
                                    LinearLayout.LayoutParams.WRAP_CONTENT);

                    image.setLayoutParams(vp);

                    cameraTakeImage.addView(image);

                    break;
                }
                default: {
                    break;
                }
            }

            return;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        showBackCamera();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                hCamera.sendEmptyMessage(0);
            }
        },200);
    }

    Handler hCamera = new Handler(new Handler.Callback() {
        @Override
        public boolean handleMessage(Message msg) {
            return false;
        }
    });



    @Override
    protected void onPause() {
        super.onPause();
        removePreview();
        releaseCamera();
    }

    private void showBackCamera() {
        releaseCamera();
        camera = getCameraInstance(Camera.CameraInfo.CAMERA_FACING_BACK);
        if (camera != null) {
            if (previewSurfaceView != null) {
                removePreview();
            }
            Constant.BACK_CAMERA_IN_USE = true;
            attachCameraToPreview();
        }
    }

    private void showFrontCamera() {
        int numberOfCameras = Camera.getNumberOfCameras();

        if (numberOfCameras > 1) {
            releaseCamera();
            camera = getCameraInstance(Camera.CameraInfo.CAMERA_FACING_FRONT);
            Constant.BACK_CAMERA_IN_USE = false;
            removePreview();
            attachCameraToPreview();
        } else {
            Toast.makeText(this, "Front camera not available", Toast.LENGTH_SHORT).show();
        }
    }

    boolean isHasFlash() {
        if (camera == null || camera.getParameters() == null
                || camera.getParameters().getSupportedFlashModes() == null) return false;
        List<String> supportedModes = camera.getParameters().getSupportedFlashModes();
        return supportedModes.size()>1;
    }

    private void attachCameraToPreview() {
        previewSurfaceView = new PreviewSurfaceView(this, camera);
        previewFrame.addView(previewSurfaceView);
        hasFlash = isHasFlash();
        if(flashBtn!=null)flashBtn.setVisible(hasFlash);
    }

    private void removePreview() {
        try {
            previewFrame.removeView(previewSurfaceView);
        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    private void releaseCamera(){
        if (camera != null){
            camera.release();        // release the camera for other applications
            camera = null;
        }
    }

    public Camera getCameraInstance(int cameraId){
        Camera c = null;
        try {
            c = Camera.open(cameraId); // attempt to get a Camera instance
        }
        catch (Exception e){
            // Camera is not available (in use or does not exist)
            e.printStackTrace();
        }
        return c; // returns null if camera is unavailable
    }

    private Camera.PictureCallback pictureCallback = new Camera.PictureCallback() {

        @Override
        public void onPictureTaken(byte[] data, Camera camera) {

            captureButton.setEnabled(false);

            if (Constant.isShapeBitmap) {
                Constant.isShapeBitmap = !Constant.isShapeBitmap;

                camera.stopPreview();

                new ImageDecodeTask(CameraActivity.this, data, previewFrame.getHeight(), previewSurfaceView.getHeight()).execute();
            } else {
                Bitmap picture = BitmapFactory.decodeByteArray(data, 0, data.length);

                Bitmap rotatedBitmap = BitmapHelper.rotateBitmap(picture, Constant.BACK_CAMERA_ROTATION,
                        previewFrame.getHeight(), previewSurfaceView.getHeight());

                imageFull.setImageBitmap(rotatedBitmap);
                cameraPreview.setVisibility(View.GONE);
                cameraTakeImage.setVisibility(View.VISIBLE);
            }
        }
    };

    @Override
    public void onForward(View v) {
        super.onForward(v);
        Bitmap bitmap;
        View v1 = findViewById(R.id.squareRoot);
        v1.setDrawingCacheEnabled(true);
        bitmap = Bitmap.createBitmap(v1.getDrawingCache());
        v1.setDrawingCacheEnabled(false);
        new SavePicToFileTask().execute(bitmap);
    }

    private class SavePicToFileTask extends AsyncTask<Bitmap,Void,String> {
        Bitmap bitmap;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgressDialog(getString(R.string.save_picture));
        }

        @Override
        protected String doInBackground(Bitmap... params) {
            String fileName = null;
            try {
                bitmap = params[0];

                String picName = TimeUtils.dtFormat(new Date(), "yyyyMMddHHmmss") + ".jpg";
                fileName = ImageUtils.saveToFile(FileUtils.getInst().getPhotoSavedPath() + "/"+ picName, false, bitmap);

            } catch (Exception e) {
                e.printStackTrace();
                toast(getString(R.string.image_processing_error), Toast.LENGTH_LONG);
            }
            return fileName;
        }

        @Override
        protected void onPostExecute(String fileName) {
            super.onPostExecute(fileName);
            dismissProgressDialog();
            navigateToNextActivity(Uri.fromFile(new File(fileName)));
        }
    }


    private void navigateToNextActivity(Uri uri) {
        Intent newIntent = new Intent(this, ApplyEffectsActivity.class);
        newIntent.setData(uri);
        this.startActivity(newIntent);
    }


    Bitmap composite(Bitmap bitmap1,Bitmap bitmap2) {

        Bitmap resultingImage=Bitmap.createBitmap(bitmap1.getWidth(), bitmap1.getHeight(), bitmap1.getConfig());

        Canvas canvas = new Canvas(resultingImage);

        // Drawing first image on Canvas
        Paint paint = new Paint();
        canvas.drawBitmap(bitmap1, 0, 0, paint);

        // Drawing second image on the Canvas, with Xfermode set to XOR
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.XOR));
        canvas.drawBitmap(bitmap2, 0, 0, paint);

        return resultingImage;
    }

}
